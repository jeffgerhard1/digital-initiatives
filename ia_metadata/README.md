# Internet Archive metadata

Please see the background discussion about these scripts on the related [Google Doc](https://docs.google.com/document/d/1Jbo555VlDzKv35Mc7kfi2A51g5Kx6VSdjcgx-FWhFQE/edit?usp=sharing).

The code in this folder is for querying the Internet Archive via API to gather information about our items, then updating the digitization database with relevant info. There are two versions here, one in active use and one that has been superseded.

Since we are querying and updating the database, this script requires the VPN.

## Current policy

### Running manually

Run the `ia_metadata.py` file as follows:

```
ia_metadata.py metadata
    This will run the main program to:
        - pull metadata from IA
        - Update database tables as needed

ia_metadata.py identifiers
    This will go through all tasks for all users
    and find out if there are any new items previously
    unidentified
    
ia_metadata.py loanhistory
    This will parse all the tasks and try to identify lendingbot changes

ia_metadata.py loanstatus
    This will simply look for any updates to loan status (for future
    use in updating Alma availability status)
    
ia_metadata.py views
    Try to use the item view API and gather view stats into the ia_views table

ia_metadata.py files
    Parse all of the items on IA and track their files via the ia_files table
```

### Quasi-automation

Run the batch scripts (`identifiers.bat`, etc.) on a regular basis, perhaps by Windows Scheduler.

### Issues

- I haven't found a good solution for running these scripts silently (without ugly command-line terminals popping up). Most likely I could do it in pure Python?
- This should really be run on a server via cron job (or equivalent) instead of a local machine)

## Superseded version

In this repo we have two main Python scripts, both of which are revisions of older scripts and could use some serious clean-up and reconfiguration. 

The first script, here called `1_ia_metadata.py`, will:

1. compile a list of Internet Archive identifiers by examining the task history of a pre-defined list of users; 
2. compile item metadata for that list and save as a csv;
3. ask if you want to save the list of identifiers (I never do anymore);
4. ask if you want to save the data in JSON format (I always do now);
5. display some summary data (not necessarily totally accurate!).
	
The second script, here called `2_update_database.py`, will:

1. read a given JSON file (I use the most recent one saved in the first script);
2. go through and map fields from the file to current database fields;
3. read the database and see if it all the data is accurate;
4. if not, update the db (both Item and Internet Archive tables) and print the changes to screen;
5. [NEW!] send a notification to a Slackbot for major updates (like a newly-recorded loan).

The code is commented up, but reflects my changing styles and skill level in Python as I changed it around over the course of a couple years.

## Requirements

Python 3, including non-standard modules:

- [internetarchive](https://github.com/jjjake/internetarchive)
- [pyodbc](https://github.com/mkleehammer/pyodbc)
- [tenacity](https://github.com/jd/tenacity)
	
