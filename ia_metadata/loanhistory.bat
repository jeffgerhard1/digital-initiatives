@echo off
echo "**** Running loan history script. Please don't close this window. ****"
call pythonw E:\j\Documents\git_repos\digital-initiatives\ia_metadata\ia_metadata.py loanhistory 1>logs\loanhistory.log 2>logs\loanhistory_errors.log
exit