# -*- coding: utf-8 -*-
"""
@author: gerhardj

This is rather old code that I lightly edited to make work with the revised
digitization database from Oct. 2018.

Essentially, it opens up a json file (the one saved in the ia_metadata.py script)
and then scans through it, maps some fields to new names in the database,
and updates database fields.

It checks each item and looks at the db to see if any changes need to be made.
If not, it just goes on to the next one.

This is not a very efficient script and running it will lock up the database
for all users for 5 minutes or more. Not ideal.

In the future, I'd like to clean it completely, integrate with the other script,
and also do some notification and statistics work, like loggin info that has
changed and generating a report. (e.g., new info today, new info this week, etc.)

I did play around with a Slack integration that actually works, so I am including
that here. This is a brand-new feature from November 2018... works as a demo
of the kind of functionality that a full workflow tool would include.


"""

import pyodbc
from tkinter.filedialog import askopenfilename
import json
import internetarchive as ia
from dateutil.parser import parse
import datetime
import requests


def update_db(cursor, params):  # borrowed from my update db program, 2018
    results = False
    if len(params['searches']) == 1:  # stick with just this for now!
        cursor.execute("""
        SELECT UID
        FROM Item_Entry
        WHERE """ + params['searches'][0][0] + """=?;
        """, params['searches'][0][1])
        try:
            row = cursor.fetchone()
            UID = row.UID
        except:
            return False
        if row:
            for change in params['changes']:
                cursor.execute("""
                UPDATE Item_Entry
                SET """ + change[0] + """=?
                WHERE UID=?;
                """, change[1], UID)
            print(UID, 'updating...')
            results = True
    return results


def updateIfNew(cursor, changed, field, newvalue, uid,
                table='Item_Entry', suppress=False):
    if table == 'Internet_Archive':
        uidstr = 'identifier'
    else:
        uidstr = 'UID'
    change = False
    query = 'SELECT ' + field + ' FROM ' + table
    query += ' WHERE ' + uidstr + '=?;'
    cursor.execute(query, uid)
    while True:
        row = cursor.fetchone()
        if row is None:
            # print('Database error')
            break
        oldvalue = row.__getattribute__(field)
        if type(newvalue) == list:
            newvalue = ','.join(newvalue)
        if type(oldvalue) == int:
            newvalue = int(newvalue)
        if type(oldvalue) == datetime.datetime:
            if oldvalue != parse(newvalue).replace(tzinfo=None):  # strip out timezone data from the datestamps!!
                change = True
        else:
            if oldvalue != newvalue:
                change = True
    if change is True:
        if changed is False:
            print('\n')
            print('----', uid, '---')
        print(field, 'updated from', oldvalue, 'to', newvalue, '(' + table + ')')
#        print('old data type:', type(oldvalue))
#        print('new data type:', type(newvalue))
        query = 'UPDATE ' + table + ' SET ' + field + '=?'
        query += ' WHERE ' + uidstr + '=?;'
        cursor.execute(query, newvalue, uid)
        slackmsg = checkfornotification(uid, field, newvalue, oldvalue)
        if slackmsg:
            postToSlack(slackmsg)
    return change


def updateDigDB(cursor, info, everything=True):
    # map the fields to the database:
    itemmap = [('scandate_access', 'Date_digitized'),
                ('has_MARC', 'IA_metadata'),
                ('format_of_access', 'Format_of_access'),  # HAVE TO ADD PDF INTO Format_of_access
                ('imagecount', 'Number_of_images'),
                ('scribed', 'Scribe'),
                ('lending_pdf', 'Borrowable'),
                ('item_equipment', 'Equipment'),
               # ('identifier_access', 'IA_URL'),
                ('has_pdf', 'IA')]
    iamap = [('addeddate_access', 'addeddate'),
             ('identifier-access', 'identifier_access'),
             ('loans__status__status', 'loans_status'),
             ('loans__status__num_loans', 'loans_number_loans'),
             ('loans__status__num_waitlist', 'loans_num_waitlist'),
             ('loans__status__last_loan_date', 'loans_last_loan_date'),
             ('externalids', 'external_identifier'),
             ('has_MARC', 'has_marc'),
             ('page-progression', 'page_progression'),
             ('scandate_access', 'scandate'),
             ('publicdate_access', 'publicdate'),
             ('republisher_date_access', 'republisher_date'),
             ('sponsordate_access', 'sponsordate'),
             ('updatedate_access', 'updatedate')
            ]
    ianonmapped = ['collection', 'contributor', 'docket', 'equipment',
                   'foldoutcount', 'imagecount', 'is_dark',
                   'has_pdf', 'has_marc', 'lending_epub', 'lending_pdf',
                   'language', 'mediatype', 'neverindex', 'noindex', 'notes', 'operator',
                   'scanner', 'scanningcenter', 'shiptracking', 'sponsor',
                   'title', 'uploader', 'volume']
    for item in info:
        if 'identifier' not in item:
            item['identifier'] = item['search_id']
        if 'has_pdf' in item:
            if item['has_pdf'] is True:
                item['format_of_access'] = 'PDF'
        if 'scribed' in item:
            if item['scribed'] is True:
                if 'operator' in item:
                    if item['operator'] =='operator1.georgetown@archive.org':  # there are some non-scribed books that have this for some reason
                        item['item_equipment'] = 'Scribe1'
        if everything is False:
            if item['identifier'].startswith('dc_circ_') or item['identifier'].startswith('2nd_circ_'):
                pass  # skip microfilm and microfiche scans // can log as desired though
            else:
                changed = False
                cursor.execute("""
                SELECT identifier_access
                FROM Internet_Archive
                WHERE identifier=?;
                """, item['identifier'])
                row = cursor.fetchone()
                if row is None:
                    print('\n***Adding', item['identifier'], 'to IA table')
                    cursor.execute("""
                    INSERT into Internet_Archive(identifier)
                    values (?)""", item['identifier'])
                for i in itemmap:
                    if i[0] in item:
                        #print('checking Item table', i[0])
                        if(updateIfNew(cursor, changed, i[1], item[i[0]], item['identifier'])) is True:
                            changed = True
                for i in iamap:
                    if i[0] in item:
                        #print('checkin IA table', i[0])
                        if(updateIfNew(cursor, changed, i[1], item[i[0]], 
                                       item['identifier'],
                                       table='Internet_Archive',
                                       suppress=True)) is True:
                            changed = True
                for i in ianonmapped:
                    if i in item:
                        #print('checking IA table II', i)
                        if(updateIfNew(cursor, changed, i, item[i], 
                                       item['identifier'],
                                       table='Internet_Archive',
                                       suppress=True)) is True:
                            changed = True

    cursor.commit()



def updateIA(cursor, info, skip=0):  # not currently in use for anything
    for idx, i in enumerate([x for x in info if 'is_dark' not in x]):
        if idx >= skip:
            cursor.execute("""
            SELECT ItemID
            FROM Item_Entry
            WHERE UID=?;
            """, i['uid'])
            row = cursor.fetchone()
            name = str(idx) + '/' + str(len(info)) + '. ' + i['search_id']
            if not row:
                print(name, i['search_id'], 'not in items table')
            else:
                item = ia.get_item(i['search_id'])
                try:
                    metadata = item.item_metadata['metadata']
                except:
                    print(name, 'not found')
                if 'gull_item' in metadata:
                    # print(name, 'already has GUL item id')
                    pass
                else:
                    r = item.modify_metadata(dict(gull_item=row.ItemID))
                    if r.status_code == 200:
                        print(name, 'updated')
#                if 'alt_title' in metadata:
#                    r = item.modify_metadata(dict(alt_title='REMOVE_TAG'))
#                    if r.status_code == 200:
#                        print(i['search_id'], ' - removed alt title')
#                if 'uploader' in metadata:
#                    if metadata['uploader'] == 'gerhardj@law.georgetown.edu' and 'noindex' in metadata:
#                        if 'collection' in metadata:
#                            if 'browserlending' not in metadata['collection']:
#                                if 'globallibraries' not in metadata['collection']:
#                                    print(i['search_id'], 'missing globallibraries')
#                                    _ = input('update it? ')
#                                    if _.lower() == 'y':
#                                        c = metadata['collection']
#                                        c.append('globallibraries')
#                                        r = item.modify_metadata(dict(collection=c))
#                                        if r.status_code == 200:
#                                            print(name, 'updated')
#                                        else:
#                                            print(r)

def postToSlack(payload):
    url = 'https://hooks.slack.com/services/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'
    headers = {'Content-Type': 'application/json'}
    requests.post(url, headers=headers, json=payload)   


def checkfornotification(uid, field, newvalue, oldvalue):
    result = None
    if field == 'loans_last_loan_date':
        result = {'attachments': [{'fallback': 'A new loan was detected for ' + uid + '!',
                                    'color': '#36a64f',
                                    'title': 'New CDL loan detected for ' + uid + '!',
                                    'title_link': 'https://archive.org/details/' + uid,
                                    'fields': [{'title':'new loan date',
                                            'value':str(newvalue),
                                            'short':True},
                                           {'title':'prev. loan date',
                                            'value':str(oldvalue),
                                            'short':True}],
                                    'thumb_url': 'https://archive.org/download/' + uid + '/page/title_w100.jpg'}]}
####### this following one works but is maybe inaccurate? "republishing" is not the same as gold package completion?
#    if field == 'republisher_date':
#        result = {'attachments': [{'fallback': 'Gold package processors finished ' + uid + '.',
#                                    'color': '#439FE0',
#                                    'title': 'Gold package processors completed ' + uid,
#                                    'title_link': 'https://archive.org/details/' + uid,
#                                    'fields': [{'title':'completed at',
#                                            'value':str(newvalue),
#                                            'short':True}],
#                                    'thumb_url': 'https://archive.org/download/' + uid + '/page/title_w100.jpg'}]}        
    return result


if __name__ == "__main__":
    print('\nThis is a program to update the digitization database. '
          'You have to be on the VPN!\n\n')
    input('First hit enter and then load up a json file of item data: ')
    f = askopenfilename(title='Choose the json file of item data')
    with open(f, 'r', encoding='utf-8') as fh:
        info = json.loads(fh.read())
    cnxn = pyodbc.connect('DSN=Digitization;Trusted_Connection=Yes;APP=Python;DATABASE=digitization')
    cursor = cnxn.cursor()
    if input('Update db? (y/n) ').lower() == 'y':
        if input('Including microfilm? ').lower() == 'y':
            ans = True
        else:
            ans = False
        updateDigDB(cursor, info, everything=ans)
    cursor.close()
#    if input('Update IA? (y/n) ').lower() == 'y':
#        updateIA(cursor, info, skip=1300)





