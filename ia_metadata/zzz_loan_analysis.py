''' ok i wanna :
    - go through the lendingbot changes data (maybe just read from the database?)
    - for each item listed, go through and see when they flip from AVAILABLE to UNAVAILBABLE
    - count for each FY
    - see how long they are loaned for
    - do some data visualization!'''

import pyodbc
import pandas
from datetime import datetime, timedelta, timezone
from dateutil.parser import parse
# import arrow

def chop_microseconds(delta):  # via https://stackoverflow.com/a/18470628
    return delta - datetime.timedelta(microseconds=delta.microseconds)


dsn = 'DSN=Digitization-RDS;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'
cnxn = pyodbc.connect(dsn)
#cursor = cnxn.cursor()

sql = '''SELECT 
        identifier, timestamp, loans__status__status, loans__status__last_loan_date,
        YEAR(DATE_ADD(timestamp, INTERVAL 6 MONTH)) AS Fiscal_year
 FROM ia_lendingbot_changes;'''
df = pandas.read_sql(sql, cnxn)  # thanks! to https://stackoverflow.com/a/46231360

data_sorted = df.sort_values(['identifier', 'timestamp'])

items = list()
loans = list()

for row in data_sorted.itertuples():
    if row.identifier not in items:
        items.append(row.identifier)
        status = 'AVAILABLE'
        startdate = None
        enddate = None
    if row.loans__status__status in ['AVAILABLE', 'UNAVAILABLE']:
        if status != row.loans__status__status:  # identify status flips?
            if row.loans__status__status == 'UNAVAILABLE':  # start of a new loan?
                if not pandas.isnull(row.loans__status__last_loan_date):  # try filtering out ones that don't have this?
                    startdate = row.timestamp
                    status = row.loans__status__status
            elif row.loans__status__status == 'AVAILABLE':  # end of a loan
                enddate = row.timestamp
                status = row.loans__status__status
                td = (enddate.round(freq='s')) - (startdate.round(freq='s'))
                # print(td, type(td))
                # fy = arrow.get(startdate.to_pydatetime()).shift(months=+6).year
                loans.append((row.identifier, startdate, enddate, int(td.total_seconds()), str(td), row.Fiscal_year))

loandf = pandas.DataFrame(loans, columns=['identifier', 'startdate', 'enddate', 'seconds', 'td', 'FY'])
loandf.to_csv('ia_metadata/logs/loans.log')
