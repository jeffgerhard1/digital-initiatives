''' 

go thru our collection and find all the metadata fields in our items' metadata


'''

import pyodbc
from internetarchive import get_tasks, get_item

allfields = list()

dsn = 'DSN=Digitization-RDS;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'

with pyodbc.connect(dsn) as cnxn:
    cursor = cnxn.cursor()
    cursor.execute('SELECT ia_tracking_identifier FROM ia_tracking WHERE dark=? AND blacklist=?', (False, False))
    rows = cursor.fetchall()
    identifiers = [_[0] for _ in rows]

for item in identifiers:
    m = get_item(item).item_metadata
    if 'metadata' in m:
        md = m['metadata']
        #print('parsing', item)
        for _ in md:
            if _ not in allfields:
                allfields.append(_)
                print(_)
    else:
        print('** NO METADATA FOUND FOR ', item)

    # with open('files/fields.txt', 'w') as fh:
    #     fh.write('\n'.join(allfields))

with pyodbc.connect(dsn) as cnxn:
    cursor = cnxn.cursor()
    cursor.execute('SELECT ia_item_fields_fieldname FROM ia_item_fields')
    rows = cursor.fetchall()
    for field in allfields:
        if field not in [_[0] for _ in rows]:
            cursor.execute('INSERT INTO ia_item_fields(ia_item_fields_fieldname) VALUES (?)', (field))
            print('adding', field, 'to fields table.')
    for dbfield in [_[0] for _ in rows]:
        if dbfield not in allfields:
            print(dbfield, ' not found in any of our items!')