''' IA metadata script for python 3.5

This is an old and hacked-together thing much in need of updating.

Basically, this is code to export a csv containing various metadata and status info from archive.org

1.  takes a defined list of users and polls their histories via the IA task
    catalog feature to compile a list of identifiers

2.  examines the metadata of each item and pulls together a big dictionary
    of metadata (and metadata-esque) stuff that may be useful

3.  saves a csv file suitable for use with the present Access database and/or
    sql commands interacting with it

4.  prints a few statistics; offers the choice to export the identifier list
    and to export the info as json

TO DO:

- split into two parts -- one to check tasks and compile list of id's; one to pull down metadata
- run these two things side-by-side on a regular basis. 1x/day for tasks; nonstop for metadata
- pull down a shadow copy of the db to compare data to so we don't have to leave the connexion on
- when there is a new update, modify the db and also send alerts as needed (e.g. with slack)
- look out for problems
- suggest fixes to things as they emerge (like add 'globallibraries' to a collection)
- note that only a few things need to be updated in real time -- like new scan info 
  and maybe lending info. The rest can be done whenever (i.e. in off-peak hours)

'''

import json
import csv
from tkinter.filedialog import asksaveasfilename
from internetarchive import get_tasks, get_item
import sys
from dateutil.parser import parse
import datetime
now = datetime.datetime.now()
from tenacity import retry, wait_random_exponential

def tasks(u, s_cmd):
    ''' function to return a deduped list of identifiers for a given user and task type '''
    i = []
    l = get_tasks(params={'search_submitter': u, 'history': '2000',
                          'limit': '10000', 'search_cmd': s_cmd + '.php'})
    # 'history' is the number of days -- note that we have a dictionary
    # from 9/2013
    # 'limit' is a total maximum for each user
    # other parameters can be figured out from the URL parameters in
    # IA catalog searches, like https://catalogd.archive.org/catalog.php?search_identifier=&search_task_id=&search_server=&search_cmd=&search_submittime=&search_submitter=gerhardj%40law.georgetown.edu&search_args=&limit=10000&history=732&searching=%21

    # the @jeffgerhard, etc usernames come up in results -- let's delete them:
    for c in l:
        if c.identifier[0] != '@':
            i.append(c.identifier)
    # also there is some other junk in the results we could drop later...
    return sorted(set(i))


def getIdentifiers(users):
    """ pulls identifiers via IA catalog lists for a list of users"""
    identifiers = []
    for u in users:
        identifiers += tasks(u, 'archive')
        identifiers += tasks(u, 'derive')
    return sorted(set(identifiers))  # set is a nice command to dedupe a list!

# the retry bit below comes from TENACITY which is helpful for laggy API calls
# see https://github.com/jd/tenacity
@retry(wait=wait_random_exponential(multiplier=1, max=120))
def pollMetadata(uid):
    ''' try to pull all metadata from a file via the get_item ia function '''
    md = get_item(uid).item_metadata
    return md

def getMetadata(uid):
    uppermd = pollMetadata(uid)
    # there is "upper-level metadata" and also "metadata metadata" within that
    # I'm using uppermd to hold the upper-level stuff 

    results = dict()  # a dictionary to hold the metadata 
    results['search_id'] = uid
    
    # First check to see if it's dark:
    # this comes in the upper-level IA metadata;
    # the API will not return any more info so we can halt if dark:
    
    if 'is_dark' in uppermd:
        results['is_dark'] = True
        return results  # just return the fact that it's dark
    else:
        results['is_dark'] = False

    # the 'files' section is interesting but the only things I think we
    # really need are MARC records (in case we never sent one) and
    # maybe checksums for the jp2 files of Scribed master files

    if 'files' in uppermd:
        for f in uppermd['files']:
            if f['format'] in ["MARC Binary", "MARC"]:
                results['has_MARC'] = True
            if f['format'] == 'Single Page Original JP2 Tar':
                results['scribe_jp2_sha1'] = f['sha1']
                results['scribe_jp2_md5'] = f['md5']
            if f['format'] in ["JPEG-Compressed PDF", "Text PDF",
                               "Additional Text PDF", "Image Container PDF"]:
                results['has_pdf'] = True

    # the 'metadata' metadata is where most of what we need is

    if 'metadata' in uppermd:
        md = uppermd['metadata']
        #  use the md variable to hold this level of metadata
        if 'collection' in md:
            if 'georgetown-university-law-library' not in md['collection']:
                return ''

        for _ in ['identifier', 'volume', 'imagecount', 'docket', 'notes', 'publisher',
                  'shiptracking', 'page-progression', 'equipment', 
                  'identifier-access', 'gull_item', 'title', 'loans__status__status',
                  'loans__status__num_loans', 'loans__status__num_waitlist',
                  'loans__status__last_loan_date', 'contributor', 'foldoutcount',
                  'language', 'mediatype', 'operator', 'scanner', 'scanningcenter', 
                  'sponsor', 'uploader']:
            if _ in md:
                results[_] = md[_]
        if 'noindex' in md:
            results['noindex'] = True
        else:
            results['noindex'] = False
        if 'neverindex' in md:
            results['neverindex'] = True

        # grab any useful date info
        for _ in ['publicdate', 'addeddate', 'updatedate', 'scandate', 'republisher_date']:
            if _ in md:
                dt = parse(md[_])
                results[_ + '_iso'] = dt.replace(tzinfo=datetime.timezone.utc).isoformat()
                if dt.hour != 0 and dt.minute != 0:
                    dt -= datetime.timedelta(hours=5)
                results[_ + '_access'] = dt.strftime("X%m/X%d/%Y %H:%M:%S").replace('X0','X').replace('X','')

        if 'scandate' in md:
            results['scribed'] = True
        if 'sponsordate' in md:
            dt = parse(md['sponsordate'])
            results['sponsordate_iso'] = dt.isoformat()
            results['sponsordate_access'] = dt.strftime("X%m/X%d/%Y").replace('X0','X').replace('X','')

        # metadata collection part has info about lendability

        if 'collection' in md:
            results['collection'] = md['collection']
            if 'georgetown-university-law-library-rr' in md['collection']:
                results['rr_collection'] = True
            else:
                results['rr_collection'] = False
#            if 'browserlending' in x['collection']:
#                results['browserlending'] = True
##### in mid-March 2018, IA switched away from this 'browserlending' collection
##### and apparently rely only on the 'inlibrary' collection...
                
            if 'inlibrary' in md['collection']:
                results['browserlending'] = True
            else:
                results['browserlending'] = False
            if 'external-identifier' in md:
                externalids = list()  # a blank list for external ids
                if 'inlibrary' in md['collection']:
                    if len(md['external-identifier']) > 8:  # assume not a list
                        externalids.append(md['external-identifier'])
                    else:
                        for _ in md['external-identifier']:
                            externalids.append(_)
#                for _ in externalids:
#                    if _[0:8] == 'acs:epub':
#                        results['lending_epub'] = True
#                    if _[0:7] == 'acs:pdf':
#                        results['lending_pdf'] = True
                for _ in externalids:
                    if 'epub' in _:
                        results['lending_epub'] = True
                    if 'pdf' in _:
                        results['lending_pdf'] = True
                if len(externalids) > 0:
                    results['externalids'] = externalids
        return results


def update_progress(counter, listtotal, i):
    # adapted from http://blender.stackexchange.com/a/30739
    i = i[0:41]
    sys.stdout.write('\r' + ' ' * 79)
    sys.stdout.flush()
    progress = counter/float(listtotal)
    length = 22  # modify this to change the length
    block = int(round(length*progress))
    spaces = 45-length
    spaces -= len(i)
    msg = "\r[{}] ".format("#"*block + "-"*(length-block))
    msg += '{}/{}'.format(counter, listtotal)
    if progress >= 1:
        msg += ' DONE' + ' ' * 35 + '\r\n'
    else:
        msg += ': ' + i + ' ' * spaces
    sys.stdout.write(msg)
    sys.stdout.flush()



localdir = r'G:\Team Drives\Digital Initiatives\Internet Archive\IA metadata\metadata history'

# if input('Pull identifiers from existing list? ').lower() == 'y':
#     with open(r"G:\Team Drives\Digital Initiatives\Internet Archive\IA metadata\metadata history\partial_IA_output2018-12-17-nomicroform.json", 'r') as fh:
#         old_data = json.loads(fh.read())
#     ids = list()
#     for item in old_data:
#         ids.append(item['search_id'])
#     ids += ['statesupport_baye_2007_000_8665274', 'theologyofth_neus_2001_000_7080715',
#             'reprodu_xxx_2007_00_3513', ' theologyinth_jaco_1975_000_4709696',
#             'understandin_henk_2008_000_10663770', 'theologyofpo_mick_1941_000_141993']
#     ids = sorted(set(ids))
# else:
users = ['gerhardj@law.georgetown.edu', 'lp627@law.georgetown.edu',
            'scribett0081@archive.org', 'cms299@law.georgetown.edu',
            'ttscribe1.georgetown@archive.org', 'lc1135@georgetown.edu']
# there are two records uploaded by cms299
print('Compiling IA task catalog history for users ', end='')
print(*users, sep=', ')

ids = getIdentifiers(users)
if input('Skip microfilm? (Y to skip, anything else will include) ').lower() == 'y':
    ids = [_ for _ in ids if _[0:7] not in ['dc_circ', '2nd_cir']]
    partial = True
else:
    partial = False

listtotal = len(ids)
filename = asksaveasfilename(
    defaultextension='.csv', initialfile='full_IA_output',
    title="Save Results As...", initialdir=localdir
    )

print('\nDownloading metadata...')

allinfo = []
with open(filename, mode='w', encoding='utf-8', newline='') as csvfile:
    fieldnames = [
        'addeddate_access',
        'addeddate_iso',
        'browserlending',
        'collection',
        'contributor',
        'docket',
        'equipment',
        'externalids',
        'foldoutcount',
        'gull_item',
        'has_MARC',
        'has_pdf',
        'identifier',
        'identifier-access',
        'imagecount',
        'is_dark',
        'language',
        'lending_epub',
        'lending_pdf',
        'loans__status__status',
        'loans__status__num_loans',
        'loans__status__num_waitlist',
        'loans__status__last_loan_date',
        'mediatype',
        'neverindex',
        'noindex',
        'notes',
        'operator',
        'page-progression',
        'publicdate_access',
        'publicdate_iso',
        'publisher',
        'republisher_date_access',
        'republisher_date_iso',
        'rr_collection',
        'scandate_access',
        'scandate_iso',
        'scanner',
        'scanningcenter',
        'scribe_jp2_md5',
        'scribe_jp2_sha1',
        'scribed',
        'search_id',
        'shiptracking',
        'sponsor',
        'sponsordate_access',
        'sponsordate_iso',
        'title',
        'updatedate_access',
        'updatedate_iso',
        'uploader',
        'volume'
        ]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames, dialect='excel')
    writer.writeheader()
    for idx, x in enumerate(ids):
        newline = getMetadata(x)
        if newline:
            writer.writerow(newline)
            allinfo.append(newline)
        update_progress(idx, listtotal, x)

listtotal = len(allinfo) #  skip any non-gull items in totals
print('\n\n')
print(now.strftime('%x'))
print('-'*45)
if partial is True:
    print('PARTIAL INFO')
else:
    print('SOME SUMMARY INFO')

# add some summary info from the allinfo dict
availabilitysummary = '''\n\nTotal number of items: {}
\tof those, {} dark, {} otherwise unavailable (pending review? a
\tMARC record uploaded but no images?), leaving {} viewable items.
'''.format(listtotal,
           len([i for i in allinfo if i['is_dark'] == True]),
           listtotal - len([i for i in allinfo if 'has_pdf' in i]),
           len([i for i in allinfo if 'has_pdf' in i]))
print(availabilitysummary)
print('Total number Scribed: ', len([i for i in allinfo if 'scribed' in i]))
print(
    'Approximate number in public domain:', len(
        [i for i in allinfo if i['is_dark'] == False and ('has_pdf', True) in i.items() and ('rr_collection', False) in i.items()]
        )
      )
print(
    'Total with "equipment" in the metadata (probably microform):', len(
        [i for i in allinfo if 'equipment' in i]
        )
      )
print(
    'Total number borrowable: ', len(
        [i for i in allinfo if ('browserlending', True) in i.items()]
        )
      )
print(
    'Total number with lending PDF: ',  len(
        [i for i in allinfo if ('lending_pdf', True) in i.items()]
        )
      )
print(
    'Total number with lending epub: ',  len(
        [i for i in allinfo if ('lending_epub', True) in i.items()]
        )
      )
print(
    'Total number with lending status: ',  len(
        [i for i in allinfo if 'loans__status__status' in i]
        )
      )
print(
    'Total number with lending status unavailable (checked out?): ',  len(
        [i for i in allinfo if ('loans__status__status', 'UNAVAILABLE') in i.items()]
        )
      )
print('\n\n' + '-'*45)
print('csv file saved.')
y = input('Do you want to export the list of identifiers? (y/n) ')
if y.lower() == 'y':
    filename = asksaveasfilename(
        defaultextension='.txt', initialfile='identifiers',
        title="Save Results As...", initialdir=localdir
    )
    with open(filename, 'w', encoding='utf-8') as fh:
        for _ in ids:
            fh.write(_ + '\n')
y = input('Do you want to export all the item data into a json file? (y/n) ')
if y.lower() == 'y':
    filename = asksaveasfilename(
        defaultextension='.json', initialfile='iteminfo',
        title="Save Results As...", initialdir=localdir
    )
    with open(filename, 'w', encoding='utf-8') as fh:
        fh.write(json.dumps(allinfo, indent=4, sort_keys=True))
