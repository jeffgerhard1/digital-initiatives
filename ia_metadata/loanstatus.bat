@echo off
echo "**** Running loan status script. Please don't close this window. ****"
call pythonw E:\j\Documents\git_repos\digital-initiatives\ia_metadata\ia_metadata.py loanstatus 1>logs\loanstatus.log 2>logs\loanstatus_errors.log
exit