# -*- coding: utf-8 -*-
"""
Note, July 2020:

* THE LOAN TRACKING FUNCTIONALITY NO LONGER WORKS.
  Around 6/17/2020, the IA changed around their methods of tracking (or not tracking) loan information,
  including loan histories and loan availabiliy status. 

Earlier notes:

Revised program to handle Internet Archive metadata, 2019/05-06

Outline:

Process 1: Generate a list of all our identifiers on IA, like once per day
    - think about any non-georgetown items, our ids and favs
    - think about dark items
- Save list of identifiers in the "files" folder

Process 2: Continuously check on 1) metadata; 2) files; 3) loans
    - Use a csv [or the database, preferably] to track when last checked:
        loans -- constantly
        metadata -- frequently
        files -- occasionally
    - Note if any new metadata fields have been added
    - Try to use the database as possible, but be wary of timeouts
    - Use some method to notice if there are problems on IA and pause for a while (maybe send an alert)
    - Update database with new info
    - Log events

PROBLEM:

IA Tasks API changed in 12/2019. Emailed Jake at archive.org and there is no real way around it but
we can use the advanced search functionality (when logged-in on a browser) to get recent items in JSON
(or whatever) format. 

E.g., 
https://archive.org/advancedsearch.php?q=collection%3Ageorgetown-university-law-library-rr&output=json&personalize=true&sort[]=addeddate+desc&sort[]=&sort[]=


"""

import internetarchive as ia
import json
import os
import csv
from dateutil.parser import parse
from datetime import datetime, timedelta, timezone
import pytz
import pyodbc
from tenacity import retry, wait_random_exponential
from dateutil.parser import parse
import sys
import ast
import requests

def tasks(u, s_cmd):
    ''' return a deduped list of identifiers for a given user u and task type s_cmd'''
    i = []
    l = ia.get_tasks(params={'search_submitter': u, 'history': '2500',
                          'limit': '10000', 'search_cmd': s_cmd + '.php'})
    # 'history' is the number of days -- note that we have a dictionary
    # from 9/2013
    # 'limit' is a total maximum for each user
    # other parameters can be figured out from the URL parameters in
    # IA catalog searches, like https://catalogd.archive.org/catalog.php?search_identifier=&search_task_id=&search_server=&search_cmd=&search_submittime=&search_submitter=gerhardj%40law.georgetown.edu&search_args=&limit=10000&history=732&searching=%21

    # the @jeffgerhard, etc usernames come up in results -- let's delete them:
    for c in l:
        if c.identifier[0] != '@' and c.identifier not in ['fav-jeffgerhard', 'fav-leah_prescott']:
            i.append(c.identifier)
    # also there is some other junk in the results we could drop later...
    return sorted(set(i))


# def getIdentifiers(dsn):
#     """ pulls identifiers via IA catalog lists for a list of users"""
#     identifiers = []
#     cnxn = pyodbc.connect(dsn)
#     cursor = cnxn.cursor()
#     cursor.execute('SELECT ia_users_users FROM ia_users')
#     rows = cursor.fetchall()
#     users = [_[0] for _ in rows]  
#     for u in users:
#         identifiers += tasks(u, 'archive')
#         identifiers += tasks(u, 'derive')
#     return sorted(set(identifiers))

# the above no longer works after 2019 API update

# let's at least grab all the public collection ones first
# and then maybe grab json files from IA to get the rest
# as per Jake's email, 1/9/2020.
# e.g. download this https://archive.org/advancedsearch.php?q=collection%3Ageorgetown-university-law-library-rr&output=json&personalize=true&sort[]=addeddate+desc&sort[]=&sort[]=
# i can get this via a requests session with my actual IA password (not the ia module's login) 
# using this technique https://pybit.es/requests-session.html
# but I still can't figure out how to paginate this thing or get more than 50.
# hmm actually the start=50 (or whatever) parameter doesn't seem to work but i can do rows=200 which is pretty good
# if I update the new books code we might be ok for a while but this is more precarious than the old way

def getIdentifiers():
    """ compile all identifiers based on a json file for our rr collection and automated ia methods for the rest """
    # this is a stopgap solution until I can figure out 
    # later I will see if I can download the json file programatically like via https://docs.python.org/3/library/http.cookiejar.html
    # Note that the ia module's session cookies do not work for this.
    with open(r"E:\j\Documents\git_repos\digital-initiatives\ia_metadata\logs\rr-collection.json") as fh:
        rr_data = json.loads(fh.read())
        if 'response' in rr_data and 'numFound' in rr_data['response']:
            identifiers = [_['identifier'] for _ in rr_data['response']['docs']]
    #
    # ok try to do this by logging in with credentials. I'm saving them in the OS environment variables
    # as "ia_login" and splitting out username and password with a | character.
    #
    # If there is a pipe character in a password this will fail! Need to make this more robust.
    #
    ia_login_details = os.environ['ia_login'].split('|')
    payload = {'username': ia_login_details[0], 'password': ia_login_details[1]}
    page = r'https://archive.org/account/login'
    browser_session = requests.Session()
    r = browser_session.get(page)
    # cookies = dict(res.cookies)
    res = browser_session.post(page, data=payload)
    advanced_search_url = r'https://archive.org/advancedsearch.php?q=collection%3Ageorgetown-university-law-library-rr&output=json&personalize=true&sort[]=addeddate+desc&rows=200'
    r = browser_session.get(advanced_search_url)
    if r.status_code == 200:
        identifiers = [_['identifier'] for _ in json.loads(r.text)['response']['docs']]
    #public_string = r'https://archive.org/advancedsearch.php?q=collection%3Ageorgetown-university-law-library&output=json&personalize=true&sort[]=addeddate+desc&sort[]=&sort[]='
    '''
https://archive.org/advancedsearch.php?q=collection%3A(georgetown-university-law-library)&fl[]=collection&fl[]=identifier&sort[]=addeddate+desc&sort[]=&sort[]=&rows=50&page=3&output=json
    '''
    public_string_1 = r'https://archive.org/advancedsearch.php?q=collection%3A(georgetown-university-law-library)&fl[]=collection&fl[]=identifier&sort[]=addeddate+desc&sort[]=&sort[]=&rows=50&page='
    public_string_2 = '&output=json'
    public_identifiers = list()
    for page_no in range(1, 8):  # this should go through ~350 most recent added
        querystring = public_string_1 + str(page_no) + public_string_2
        public_domain = ia.get_session().get(querystring)
        public_identifiers += [_['identifier'] for _ in json.loads(public_domain.text)['response']['docs']]
#    print(len(public_identifiers))
#    print(len(identifiers))
    return identifiers + public_identifiers






# with open('files/users.txt', 'r') as fh:
#     users = [_ for _ in fh.read().splitlines() if _]

# with open('files/fields.txt') as fh:
#     fields = [_ for _ in fh.read().splitlines() if _]

dsn = 'DSN=Digitization-RDS;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'
# cnxn = pyodbc.connect(dsn)
# cursor = cnxn.cursor()

def get_db_cols(dsn, table):
    cnxn = pyodbc.connect(dsn)
    cursor = cnxn.cursor()
    cursor.execute('''SELECT COLUMN_NAME
                   FROM INFORMATION_SCHEMA.COLUMNS
                   WHERE TABLE_SCHEMA = 'digitization' AND TABLE_NAME=?;''', table)
    return [_[0] for _ in cursor.fetchall()]


def add_ia_fields_to_table(fields, dsn):
    cnxn = pyodbc.connect(dsn)
    cursor = cnxn.cursor()
    # the following was to set up columns in our database. I ran out! The last 3 existing col's are excluded for now but I can delete some later and fix this
    cursor.execute('''SELECT COLUMN_NAME
                      FROM INFORMATION_SCHEMA.COLUMNS
                      WHERE TABLE_SCHEMA = 'digitization' AND TABLE_NAME = 'ia_metadata';''')
    headings = [_[0] for _ in cursor.fetchall()]
    params = list()
    for field in fields:
        if field not in headings:
            params.append(field)
    for p in params:
        sql = "ALTER TABLE `digitization`.`ia_metadata` "
        sql += "ADD COLUMN `" + p + "` varchar(191) NULL AFTER `identifier`;"
        cursor.execute(sql)

#### GENERATE A LIST OF IDENTIFIERS

# THIS HAS TO BE RUN DAILY-ISH:

def save_all_identifiers(dsn):
    identifiers = getIdentifiers()
    # with open('files/identifiers.txt', 'w') as fh:
    #     fh.write('\n'.join(identifiers))
    cnxn = pyodbc.connect(dsn)
    cursor = cnxn.cursor()
    for i in identifiers:
        found = cursor.execute('SELECT * FROM ia_tracking WHERE ia_tracking_identifier=?', i).fetchval()
        if not found:
            cursor.execute('INSERT INTO ia_tracking(ia_tracking_identifier) VALUES (?)', i)
            print('Added', i, 'to tracking table')
    cursor.commit()


##### GATHER IA INFO

# the retry bit below comes from TENACITY which is helpful for laggy API calls
# see https://github.com/jd/tenacity

@retry(wait=wait_random_exponential(multiplier=1, max=120))
def pollMetadata(uid):
    ''' try to pull all metadata from a file via the get_item ia function '''
    md = ia.get_item(uid).item_metadata
    return md

def getMetadata(fields, uid):
    uppermd = pollMetadata(uid)
    # there is "upper-level metadata" and also "metadata metadata" within that
    # I'm using uppermd to hold the upper-level stuff 

    extraresults = dict()  # a dictionary to hold other ia data from the upper metadata, files, etc.
    results = dict()  # a dictionary to hold the metadata

    extraresults['identifier'] = uid

    # First check to see if it's dark:
    # this comes in the upper-level IA metadata;
    # the API will not return any more info so we can halt if dark:
    
    if 'is_dark' in uppermd:
        extraresults['is_dark'] = True
        return None, extraresults  # just return the fact that it's dark
    elif len(uppermd) < 1:
        return None, None

    # COME BACK LATER TO DO "EXTRARESULTS"

    # the 'metadata' metadata is where most of what we need is

    if 'metadata' in uppermd:
        md = uppermd['metadata']
        if 'collection' in md:
            if 'georgetown-university-law-library' not in md['collection']:
                return None, None
        for _ in fields:
            if _ in md:
                results[_] = md[_]
    return results, extraresults

# PULL IA METADATA
# THIS HAS TO BE RUN FREQUENTLY:

def list_item_fields(dsn):
    with pyodbc.connect(dsn) as cnxn:
        cursor = cnxn.cursor()
        cursor.execute('SELECT ia_item_fields_fieldname FROM ia_item_fields')
        rows = cursor.fetchall()
        fields = [_[0] for _ in rows]
    return fields

def update_metadata_and_items_tables(dsn):
    extras = list()
    # with open('files/identifiers.txt', 'r') as fh:
    #     identifiers = [_ for _ in fh.read().splitlines() if _]
    with pyodbc.connect(dsn) as cnxn:
        cursor = cnxn.cursor()
        cursor.execute('SELECT ia_item_fields_fieldname FROM ia_item_fields')
        rows = cursor.fetchall()
        fields = [_[0] for _ in rows]
        cursor.execute('SELECT ia_tracking_identifier FROM ia_tracking')
        rows = cursor.fetchall()
        identifiers = [_[0] for _ in rows]        
    cols = get_db_cols(dsn, 'ia_metadata')
    checkmicro = True
    for identifier in identifiers:
        check = True
        if checkmicro is False:
            if identifier.startswith('dc_circ') or identifier.startswith('2nd_circ'):
                check = False
        if check is True:
            update = False
            insert = False
            with pyodbc.connect(dsn) as cnxn:
                cursor = cnxn.cursor()
                cursor.execute('SELECT tracking_metadata, dark, blacklist FROM ia_tracking WHERE ia_tracking_identifier=?',
                                identifier)
                row = cursor.fetchone()
                if row:
                    if row[1] is True or row[2] is True:
                        update = False
                    elif row[0]:
                        diff = datetime.utcnow() - row[0]
                        if identifier.startswith('dc_circ') or identifier.startswith('2nd_circ'):
                            if diff > timedelta(days=2):  # 2 days since checking these
                                update = True
                            else:
                                checkmicro = False
                                print('(skipping checking all the microform files)')
                        elif diff > timedelta(hours=3):  # can finesse this later.
                            update = True
                    else:
                        update = True
                else:
                    insert = True
            if update is True or insert is True:
                results, extraresults = getMetadata(fields, identifier)
                if extraresults:
                    if 'is_dark' in extraresults:
                        extras.append(extraresults['identifier'])
                if results in [None, {}]:
                    print(identifier, 'is dark or otherwise inaccessible.')
                    with pyodbc.connect(dsn) as cnxn:
                        cursor = cnxn.cursor()
                        cursor.execute('UPDATE ia_tracking SET tracking_metadata=? WHERE ia_tracking_identifier=?',
                                        datetime.utcnow(), identifier)
                else:
                    with pyodbc.connect(dsn) as cnxn:
                        cursor = cnxn.cursor()
                        if update is True:
                            # 
                            # cursor.execute('SELECT ia_metadata_timestamp FROM ia_metadata WHERE identifier=?',
                            #                results['identifier'])
                            # row = cursor.fetchone()
                            #if row:
                                # only want to ACTUALLY update if something has changed, 
                                # but i'll come back to that later
                            if 'identifier' in results:
                                identifier = results.pop('identifier')
                            removals = list()
                            for r in results:
                                if r not in cols:
                                    print('removing', r, 'from', identifier)
                                    removals.append(r)
                            for r in removals:
                                results.pop(r)
                            cursor.execute('SELECT ia_metadata_timestamp FROM ia_metadata WHERE identifier=?', identifier)
                            mdtablerow = cursor.fetchone()
                            if not mdtablerow:
                                cursor.execute('INSERT INTO ia_metadata(identifier) VALUES (?)', identifier)
                            query = "UPDATE ia_metadata SET "
                            for i, r in enumerate(results):
                                query += '`' + r + '`=?'
                                if i < len(results) - 1:
                                    query += ', '
                            query += ' WHERE identifier=?;'
                            vals = tuple([str(_) for _ in results.values()] + [identifier])
                            cursor.execute(query, vals)
                            #print('updating', identifier)
                            cursor.execute('UPDATE ia_tracking SET tracking_metadata=? WHERE ia_tracking_identifier=?',
                                            datetime.utcnow(), identifier)
                        elif insert is True:
                            # we want to insert this one
                            results, extraresults = getMetadata(fields, identifier)
                            query = "INSERT INTO ia_metadata("
                            query += ','.join(['`' + str(_) + '`' for _ in results])
                            query += ') VALUES ('
                            query += ','.join(['?'] * len(results))
                            query += ');'
                            vals = tuple([str(_) for _ in results.values()])
                            cursor.execute(query, vals)
                            print('adding', results['identifier'])
                            cursor.execute('''INSERT INTO ia_tracking(ia_tracking_identifier, tracking_metadata)
                                            VALUES (?, ?)''', identifier, datetime.utcnow())
                updateItem(dsn, identifier)
    return extras
# GO THROUGH AND UPDATE ITEM TABLE
# Check for new scandate (plus image count, inferred scanner of Scribe)
# I can probably just call this from the other code, just run it routinely as well

def updateItem(dsn, identifier):
    with pyodbc.connect(dsn) as cnxn:
        cursor = cnxn.cursor()
        query = '''SELECT scandate, 
                    imagecount, 
                    addeddate, 
                    republisher_date, 
                    collection, 
                    loans__status__status, 
                    loans__status__num_history, 
                    loans__status__last_loan_date,
                    sponsordate, 
                    `external-identifier`,
                    scanner'''
        query += ' FROM ia_metadata WHERE identifier=?'
        cursor.execute(query, identifier)
        row = cursor.fetchone()
        if row:
            # parse the collections
            rr_collection = False
            lending_collection = False
            collections = ast.literal_eval(row[4])
            if 'inlibrary' in collections:
                lending_collection = True
            if 'georgetown-university-law-library-rr' in collections:
                rr_collection = True
            # parse the dates
            addeddate = None
            if row[2]:
                addeddate = parse(row[2]).replace(tzinfo=timezone.utc).astimezone(tz=None)
            repub_date = None
            if row[3]:
                repub_date = parse(row[3]).replace(tzinfo=timezone.utc).astimezone(tz=None)
            loancount = None
            if row[6]:
                loancount = int(row[6])
            lastloandate = None
            if row[7]:
                lastloandate = parse(row[7]).replace(tzinfo=timezone.utc).astimezone(tz=None)
            sponsordate = None
            if row[8]:
                sponsordate = parse(row[8])
            scandate = None
            if row[0]:
                scandate = parse(row[0]).replace(tzinfo=timezone.utc).astimezone(tz=None)
            imagecount = None
            if row[1]:
                imagecount = int(row[1])
            lendingpdf = False
            lendingepub = False
            if row[9]:
                if 'epub' in row[9]:
                    lendingepub = True
                if 'pdf' in row[9]:
                    lendingpdf = True
#                if '[' in row[9]:
#                    externals = ast.literal_eval(row[9])
#                else:
#                    externals = [row[9]]
#                for _ in externals:
#                    if 'epub' in _:
#                        lendingepub = True
#                    if 'pdf' in _:
#                        lendingpdf = True
            scribeid = None
            if row[10]:  # check for scanner info to identify which Scribe (if any)
                if row[10] in ['scribett0081',  # the way they originally tagged it
                               'scribett_DEV_Davide_test_9',  # not sure what this was
                               'ttScribe v1.12',  # a software version rather than a scanner id but we had this for a long time
                               'ttscribe1.georgetown.archive.org']:  # the current version
                    scribeid = 1 # original TT-Scribe
                elif row[10] == 'ttscribe2.georgetown.archive.org':
                    scribeid = 8 # second Scribe
            # HAVE TO GET PDF FROM FILE INFO OR ELSE DROP FROM TABLE
            # ALSO SPONSORDATE PARSING
            cursor.execute('SELECT Added_date FROM Internet_Archive WHERE IA_identifier=?', identifier)
            existingrow = cursor.fetchone()
            if not existingrow:
                cursor.execute("INSERT INTO Internet_Archive(IA_identifier) VALUES(?)", identifier)
            updatequery = '''UPDATE Internet_Archive SET
                    Rights_reserved_collection=?,
                    Lending_collection=?,
                    Loan_status=?,
                    IA_defined_loan_count=?,
                    Last_loan_date=?,
                    Added_date=?,
                    Scribe_scandate=?,
                    Republish_date=?,
                    Invoice_date=?,
                    Lending_EPUB=?,
                    Lending_PDF=?
                    WHERE IA_identifier=?
                    '''
            updateparams = (rr_collection,
                            lending_collection,
                            row[5],
                            loancount,
                            lastloandate,
                            addeddate,
                            scandate,
                            repub_date,
                            sponsordate,
                            lendingepub,
                            lendingpdf,
                            identifier
                            )
            cursor.execute(updatequery, updateparams)
            if cursor.execute('SELECT IA FROM Item_Entry WHERE UID=?', identifier).fetchval() is False:
                cursor.execute('UPDATE Item_Entry SET IA=? WHERE UID=?', True, identifier)
            if scandate:  # update the item table for Scribe scans
                cursor.execute('SELECT Digitization_equipment, Date_digitized, Number_of_images FROM Item_Entry WHERE UID=?', identifier)
                row = cursor.fetchone()
                if row[0] is None or row[1] is None or row[2] is None:
                    print('Updating item table for', identifier)
                    updatequery = 'UPDATE Item_Entry SET Digitization_equipment=?, Date_digitized=?, Number_of_images=? WHERE UID=?'
                    cursor.execute(updatequery, (scribeid, scandate, imagecount, identifier))
                return True
        else:
            print(identifier, 'not in item table!!')
    return False

# GRAB LOAN DATA -- RUN OCCASIONALLY (LIKE DAILY)

# NOTE: IA TASKS API UPDATED IN 12/2019... revising the following:

def lendingbottasks(session, identifier):
    tasks = ia.get_item(identifier).get_all_item_tasks(params={'catalog': 1, 'summary': 0})
    tasklog = list()
    timestring = 'Task started at: UTC:'
    for task in [t for t in tasks if t.submitter == 'lendingbot@archive.org']:
        changes = None
        timestamp = None  
        log = tasks[0].get_task_log(task.task_id, session)
        for line in log.splitlines():
            if timestring in line:
                timestamp = line[len(timestring):].split('(')[0].strip()
        if '[-changes]' in log:
            changes = [_ for _ in log.splitlines() if '[-changes]' in _][0]
        if changes:
            patches = ast.literal_eval(changes.strip()[14:])
            for p in patches:
                if p['target'] == 'metadata':
                    patch = json.loads(p['patch'])
                    status = dict()
                    status['identifier'] = identifier
                    status['timestamp'] = timestamp
                    status['taskid'] = task.task_id
                    for op in patch:
                        if op['op'] in ['add', 'replace']:
                            status[op['path'].replace('\\', '').replace('/', '')] = op['value']
                    tasklog.append(status)
    return tasklog

def getLoanData(dsn):
    with pyodbc.connect(dsn) as cnxn:
        params = list()
        lending = list()
        fields = ['identifier',
                 'loans__status__last_loan_date',
                 'loans__status__last_waitlist_date',
                 'loans__status__num_history',
                 'loans__status__num_loans',
                 'loans__status__num_waitlist',
                 'loans__status__status',
                 'taskid',
                 'timestamp']
        cursor = cnxn.cursor()
        query = '''SELECT IA_identifier FROM Internet_Archive WHERE Lending_Collection=?'''
        cursor.execute(query, 1)
        rows = cursor.fetchall()
        for row in rows:
            lending.append(row[0])
        session = ia.session.ArchiveSession()
        for book in lending:
            tasklog = lendingbottasks(session, book)
            for task in tasklog:
                cursor.execute('SELECT * FROM ia_lendingbot_changes WHERE taskid=?', task['taskid'])
                row = cursor.fetchone()
                if row:
                    pass
                    #print(task['taskid'], 'already in db.')
                else:
                    tasklist = list()
                    for f in fields:
                        if f in task:
                            tasklist.append(task[f])
                        else:
                            tasklist.append(None)
                    params.append(tuple(tasklist))
        updatequery = 'INSERT INTO ia_lendingbot_changes('
        updatequery += ', '.join(fields)
        updatequery += ') values ('
        updatequery += '?, '*(len(fields) -1) + '?'
        updatequery += ')'
        if params:
            cursor.executemany(updatequery, params)

# GRAB CURRENT LOAN STATUS -- RUN AS OFTEN AS POSSIBLE (HOURLY?)

def grabLoanStatus(dsn):
    ''' check the lending status constantly for availability (to update Alma status??) '''
    lending = list()
    params = list()
    with pyodbc.connect(dsn) as cnxn:
        cursor = cnxn.cursor()
        query = '''SELECT IA_identifier FROM Internet_Archive WHERE Lending_Collection=?'''
        cursor.execute(query, 1)
        rows = cursor.fetchall()
        for row in rows:
            lending.append(row[0])
    for identifier in lending:
        params.append((datetime.utcnow(), identifier))  # save all the timestamps to update in bulk at end
        with pyodbc.connect(dsn) as cnxn:
            cursor = cnxn.cursor()
            query = 'SELECT loan_status from ia_tracking WHERE ia_tracking_identifier=?'
            existingstatus = cursor.execute(query, identifier).fetchval()
            try:
                iastatus = ia.get_item(identifier).item_metadata['metadata']['loans__status__status']
            except KeyError:  # in case the 'metadata' is not available (like a problem item)
                print('Metadata unavailable for', identifier)
                iastatus = 'UNAVAILABLE'
            if existingstatus != iastatus:
                print('Updating', identifier, 'loan status to', iastatus)
                cursor.execute('UPDATE ia_tracking SET loan_status=? WHERE ia_tracking_identifier=?',
                              (iastatus, identifier))
                # AND IN THE FUTURE ---> TRIGGER SOME ALMA STATUS UPDATE???
    with pyodbc.connect(dsn) as cnxn:
        cursor = cnxn.cursor()
        print('Checked lending for', len(params), 'items.')
        cursor.executemany('UPDATE ia_tracking SET tracking_loans=? WHERE ia_tracking_identifier=?', params)

# GRAB VIEWS -- RUN DAILY

def ia_views(dsn):
    cnxn = pyodbc.connect(dsn)
    cursor = cnxn.cursor()
    cursor.execute('SELECT dark, blacklist, ia_tracking_identifier FROM ia_tracking')
    rows = cursor.fetchall()
    identifiers = [_[2] for _ in rows if _[0] is False and _[1] is False]
    cnxn.close()
    for identifier in identifiers:
        details, have_data, all_time, last_30day, last_7day, pre_20170101_total = getViews(identifier)
        if have_data is True:
            with pyodbc.connect(dsn) as cnxn:
                cursor = cnxn.cursor()
                cursor.execute('SELECT * FROM ia_views WHERE ia_views_identifier=?', identifier)
                result = cursor.fetchone()
                if not result:
                    cursor.execute('INSERT INTO ia_views(ia_views_identifier) VALUES (?)', identifier)
                updatequery = '''UPDATE ia_views SET ia_views_all_time=?,
                                    ia_views_last_30day=?,
                                    ia_views_last_7day=?,
                                    ia_views_pre_20170101_total=? 
                                WHERE ia_views_identifier=?'''
                cursor.execute(updatequery, all_time, last_30day, last_7day, pre_20170101_total, identifier)
            if len(details) > 0:
                params = list()
                for d in details:
                    dt = parse(d[0])
                    with pyodbc.connect(dsn) as cnxn:
                        cursor = cnxn.cursor()    
                        found = cursor.execute('SELECT ia_views_date_count FROM ia_views_log WHERE ia_views_log_identifier=? AND ia_views_date=?', (identifier, dt)).fetchval()
                        if not found:
                            params.append((identifier, dt, int(d[1])))
                if params:
                    with pyodbc.connect(dsn) as cnxn:
                        cursor = cnxn.cursor()
                        if len(params) > 1:
                            cursor.executemany('INSERT INTO ia_views_log(ia_views_log_identifier, ia_views_date, ia_views_date_count) VALUES (?, ?, ?)',
                                                params)
                        elif len(params) == 1:
                            cursor.execute('INSERT INTO ia_views_log(ia_views_log_identifier, ia_views_date, ia_views_date_count) VALUES (?, ?, ?)',
                                            params[0][0], params[0][1], params[0][2])


def getViews(identifier):
    url = r'https://be-api.us.archive.org/views/v1/long/' + identifier
    r = requests.get(url)
    if r.status_code == 200:
        result = json.loads(r.text)
        details = list()
        have_data = result['ids'][identifier]['have_data']
        all_time = result['ids'][identifier]['all_time']
        last_30day = result['ids'][identifier]['last_30day']
        last_7day = result['ids'][identifier]['last_7day']
        pre_20170101_total = result['ids'][identifier]['detail']['pre_20170101_total']
        for i, _ in enumerate(result['ids'][identifier]['detail']['non_robot']['per_day']):
            if _ > 0:
                details.append((result['days'][i], _))
    return details, have_data, int(all_time), int(last_30day), int(last_7day), int(pre_20170101_total)



# GRAB FILE DATA -- RUN OCCASIONALLY

def ia_files(dsn):
    cnxn = pyodbc.connect(dsn)
    cursor = cnxn.cursor()
    cursor.execute('SELECT dark, blacklist, ia_tracking_identifier FROM ia_tracking')
    rows = cursor.fetchall()
    identifiers = [_[2] for _ in rows if _[0] is False and _[1] is False]
    cols = get_db_cols(dsn, 'ia_files')
    cnxn.close()
    for identifier in identifiers:
        haspdf = False
        files = ia.get_item(identifier).files
        if files:
            updatecols = list()
            updatevals = list()
            foundcols = list()
            for f in files:
                fileformat = f['format'].replace(' ', '_')
                if fileformat not in cols:
                    if f['format'] == 'JPEG-Compressed PDF':  # Don't want the hyphen in db column name
                        fileformat = 'JPEG_Compressed_PDF'
                    else:
                        print('unexpected file format (' + f['format'] + ') in ' + identifier)
                        fileformat = None
                if fileformat:
                    foundcols.append(fileformat)
                    updatecols.append(fileformat)
                    updatevals.append(f['source'])
                    if fileformat == 'Single_Page_Original_JP2_Tar':  # capture the sha1 while we're here
                        updatecols.append('Single_Page_Original_JP2_Tar_sha1')
                        updatevals.append(f['sha1'])
                        foundcols.append('Single_Page_Original_JP2_Tar_sha1')
                    if 'PDF' in fileformat:
                        haspdf = True
            if foundcols:  # also clear any values that might have vanished.
                for _ in cols:
                    if _ not in foundcols and _ not in ['ia_files_identifier', 'ia_files_timestamp']:
                        updatecols.append(_)
                        updatevals.append(None)
            if updatecols:
                with pyodbc.connect(dsn) as cnxn:
                    cursor = cnxn.cursor()    
                    found = cursor.execute('SELECT ia_files_timestamp FROM ia_files WHERE ia_files_identifier=?', identifier).fetchval()
                    if not found:
                        cursor.execute('INSERT INTO ia_files(ia_files_identifier) VALUES (?)', identifier)
                        cursor.commit()
                    updatequery = 'UPDATE ia_files SET '
                    updatequery += '=?, '.join(updatecols)
                    updatequery += '=? WHERE ia_files_identifier=?'
                    updatevals.append(identifier)
                    cursor.execute(updatequery, tuple(updatevals))
                    if haspdf is True:
                        if cursor.execute('SELECT Has_PDF FROM Internet_Archive WHERE IA_identifier=?', identifier).fetchval() == False:
                            cursor.execute('UPDATE Internet_Archive SET Has_PDF=? WHERE IA_identifier=?', (True, identifier))
                        if cursor.execute('SELECT Format_of_access FROM Item_Entry WHERE UID=?', identifier).fetchval() != 1:
                            cursor.execute('UPDATE Item_Entry SET Format_of_access=? WHERE UID=?', (1, identifier))
    


if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == 'metadata':
            update_metadata_and_items_tables(dsn)
        elif sys.argv[1] == 'identifiers':
            save_all_identifiers(dsn)
        elif sys.argv[1] == 'loanhistory':
            getLoanData(dsn)  # BUT THIS STOPPED WORKING IN JUNE 2020
        elif sys.argv[1] == 'loanstatus':
            grabLoanStatus(dsn)  # BUT THIS STOPPED WORKING IN JUNE 2020
        elif sys.argv[1] == 'views':
            ia_views(dsn)
        elif sys.argv[1] == 'files':
            ia_files(dsn)
    else:
        msg = """USAGE INSTRUCTIONS:
Enter one of the following commands:
    
ia_metadata.py metadata
    This will run the main program to:
        - pull metadata from IA
        - Update database tables as needed

ia_metadata.py identifiers
    This will go through all tasks for all users
    and find out if there are any new items previously
    unidentified
    
ia_metadata.py loanhistory
    This will parse all the tasks and try to identify lendingbot changes

ia_metadata.py loanstatus
    This will simply look for any updates to loan status (for future
    use in updating Alma availability status)
    
ia_metadata.py views
    Try to use the item view API and gather view stats into the ia_views table

ia_metadata.py files
    Parse all of the items on IA and track their files via the ia_files table    
"""
        print(msg)
