@echo off
echo "**** Running identifier script. Please don't close this window. ****"
call pythonw E:\j\Documents\git_repos\digital-initiatives\ia_metadata\ia_metadata.py identifiers 1>logs\identifiers.log 2>logs\identifier_errors.log
call pythonw E:\j\Documents\git_repos\digital-initiatives\ia_metadata\ia_metadata.py files 1>logs\files.log 2>logs\files_errors.log
exit