import internetarchive as ia
import json
import os
import csv
from dateutil.parser import parse
from datetime import datetime, timedelta, timezone
import pytz
import pyodbc
from tenacity import retry, wait_random_exponential
from dateutil.parser import parse
import sys
import ast
import requests

def ia_views():
    with pyodbc.connect(dsn) as cnxn:
        cursor = cnxn.cursor()
        cursor.execute('SELECT ia_tracking_identifier FROM ia_tracking')
        rows = cursor.fetchall()
        identifiers = [_[0] for _ in rows]
    for identifier in identifiers:
        details, have_data, all_time, last_30day, last_7day, pre_20170101_total = getViews(identifier)
        if have_data is True:
            cursor.execute('SELECT * FROM ia_views WHERE ia_views_identifier=?', identifier)
            result = cursor.fetchone()
            if not result:
                cursor.execute('INSERT INTO ia_views(ia_views_identifier=?)', identifier)
            updatequery = '''UPDATE ia_views SET ia_views_all_time=?,
                                ia_views_last_30day=?,
                                ia_views_last_7day=?,
                                ia_views_pre_20170101_total=? 
                            WHERE ia_views_identifier=?'''
            cursor.execute(updatequery, all_time, last_30day, last_7day, pre_20170101_total)
            if len(details) > 0:
                for d in details:
                    dt = parse(d[0])
                    found = cursor.execute('SELECT ia_views_date_count FROM ia_views_log WHERE ia_views_log_identifier=? AND ia_views_data=dt').fetchval()
                    if not found:
                        cursor.execute('INSERT INTO ia_views_log(ia_views_log_identifier=?, ia_views_date=?, ia_views_date_count=?',
                                        identifier, dt, int(d[1]))


def getViews(identifier):
    url = r'https://be-api.us.archive.org/views/v1/long/' + identifier
    r = requests.get(url)
    if r.status_code == 200:
        result = json.loads(r.text)
        details = list()
        have_data = result['ids'][identifier]['have_data']
        all_time = result['ids'][identifier]['all_time']
        last_30day = result['ids'][identifier]['last_30day']
        last_7day = result['ids'][identifier]['last_7day']
        pre_20170101_total = result['ids'][identifier]['detail']['pre_20170101_total']
        for i, _ in enumerate(result['ids'][identifier]['detail']['non_robot']['per_day']):
            if _ > 0:
                details.append((result['days'][i], _))
    return details, have_data, int(all_time), int(last_30day), int(last_7day), int(pre_20170101_total)

