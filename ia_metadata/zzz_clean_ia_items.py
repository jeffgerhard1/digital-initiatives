'''
clean up IA stuff 
1. remove extraneous tags (these aren't doing harm but still...)
2. clean up language of non-English on records and briefs
    and assess any other non-English for accuracy
3. consider fixing bib data in old Scribe software uploads

'''

import internetarchive as ia
import pyodbc
from time import sleep
from datetime import datetime

dsn = 'DSN=Digitization-RDS;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'

# ADD MMSID TO OUR ITEMS
cnxn = pyodbc.connect(dsn)
cursor = cnxn.cursor()

cursor.execute('''SELECT UID, Date_digitized FROM Item_Entry 
                WHERE Digitization_equipment=?''',
                (4,))
rows = cursor.fetchall()

for row in rows:
    item = row[0]
    existing = ia.get_item(item)
    if existing.metadata:  # filter out dark items
        md = dict()
        md['date-digitized'] = str(row[1])
        print(f'{item}:')
        print(md)
        ia.modify_metadata(item, md)
        sleep(.3)
    else:
        print(f'NO METADATA FOUND FOR {item} (dark?)')


# cursor.execute('SELECT UID, Bib_ID, Digitization_equipment, Date_digitized FROM Item_Entry WHERE Item_Type=? AND IA=?;',
#                (1, True))

# rows = cursor.fetchall()

# for row in rows:
#     item = row[0]
#     existing = ia.get_item(item)
#     if existing.metadata:  # filter out dark items
#         if 'gulawlib-identifier' not in existing.metadata:
#             md = {'gulawlib-identifier': row[1]}
#             if row[2] == 2:
#                 md['equipment'] = 'Kirtas Kabis I'
#                 md['camera'] = 'Canon EOS 5D Mark II'
#                 md['date-digitized'] = str(row[3])
#             elif row[2] == 4:
#                 md['equipment'] = 'Zeutschel 12002Adv+'
#             elif row[2] == 1:
#                 md['camera'] = 'Nikon J4'
#             print(f'{item}:')
#             print(md)
#             ia.modify_metadata(item, md)
#             sleep(.3)
#     else:
#         print(f'NO METADATA FOUND FOR {item} (dark?)')

# TO DO: CLEAN UP METADATA FOR BOOKS SCRIBED PRE-2019


# CLEAN UP LANGUAGES

# fields = ['file', 'bookid', 'gull_item']
# cnxn = pyodbc.connect(dsn)
# cursor = cnxn.cursor()
# # cursor.execute('SELECT identifier, file, bookid, gull_item FROM ia_metadata')
# cursor.execute('SELECT identifier, language FROM ia_metadata')
# rows = cursor.fetchall()

# non_eng = list()
# for row in rows:
#     if row[1] not in ['eng', 'English']:
#         if row[1] is None:
#             non_eng.append(row[0])
#         elif "['" not in row[1]:  # we can ignore lists of languages
#             if row[0].startswith('dc_circ') or row[0].startswith('2nd_circ'):
#                 non_eng.append(row[0])
#             else:
#                 print(row)


# CLEAN UP MESSY METADATA IN ITEMS:

# for item in non_eng:
#     cursor.execute('select dark, blacklist from ia_tracking where ia_tracking_identifier=?', (item,))
#     row = cursor.fetchone()
#     if row[0] or row[1]:
#         print('skipping', item)
#     else:
#         md = {'language': 'eng'}
#         ia.modify_metadata(item, md)

# rows = cursor.fetchall()
# for row in rows:
#     md = dict()
#     if row[1]:
#         md['file'] = 'REMOVE_TAG'
#     if row[2]:
#         md['bookid'] = 'REMOVE_TAG'
#     if row[3]:
#         md['gull_item'] = 'REMOVE_TAG'
#     if md:
#         print(row[0])
#         print(md)
#         print(ia.modify_metadata(row[0], md))
#         sleep(.8)