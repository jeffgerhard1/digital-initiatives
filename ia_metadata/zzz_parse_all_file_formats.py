''' 

go thru our collection and find all the formats in our items' metadata


'''

import json
import internetarchive as ia
import pyodbc



dsn = 'DSN=Digitization-RDS;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'

with pyodbc.connect(dsn) as cnxn:
    cursor = cnxn.cursor()
    cursor.execute('SELECT ia_tracking_identifier FROM ia_tracking WHERE dark=? AND blacklist=?', (False, False))
    rows = cursor.fetchall()
    identifiers = [_[0] for _ in rows]

formats = list()

for identifier in identifiers:
    item = ia.get_item(identifier)
    files = item.files
    if files:
        for f in files:
            if f['format'] not in formats:
                formats.append(f['format'])
                print(f['format'])

           