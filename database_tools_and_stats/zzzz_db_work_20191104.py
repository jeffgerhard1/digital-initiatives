import pyodbc

# setup odbc connection:
dsn = 'DSN=Digitization-RDS;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'
cnxn = pyodbc.connect(dsn)
cursor = cnxn.cursor()
sql = '''
    SELECT Item_Extended_UID, Cases_Docket, Item_Extended_Notes
    FROM
    Items_Extended
    WHERE
    Cases_Multi_Docket=? AND Cases_Court=?'''

cursor.execute(sql, (False, 13))
rows = cursor.fetchall()
for row in [r for r in rows if r[1] is None]:
    if row[2]:
        print(f'NOTE FOUND for {row[0]}!')
        print(row[2])
    else:
        docket = row[0].split('_')[3]
        query = 'UPDATE Items_Extended SET Cases_Docket=? WHERE Item_Extended_UID=?'
        cursor.execute(query, (docket, row[0],))
