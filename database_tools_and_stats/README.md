# Database integrations and tools

_Tools for querying the database._

### Jupyter notebooks

- A series of notebooks for compiling database data into dataframes and updating Google Sheets with the results.

### Search the database for an item or bibliographic record

A `db_search.py` file that can query the database by barcode, UID, or MMSID and return useful data and links.

Run it like:

`python db_search.py` and then enter in a barcode, a UID, or an MMSID.

There is also a batch file that launches this. You could create a desktop shortcut from that.
