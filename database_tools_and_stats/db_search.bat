@echo off
if exist %LOCALAPPDATA%\Continuum\anaconda3\ set root=%LOCALAPPDATA%\Continuum\anaconda3
if exist %LOCALAPPDATA%\Continuum\miniconda3\ set root=%LOCALAPPDATA%\Continuum\miniconda3
if exist %UserProfile%\Miniconda3\ set root=%UserProfile%\Miniconda3
call %root%\Scripts\activate.bat %root%
python db_search.py