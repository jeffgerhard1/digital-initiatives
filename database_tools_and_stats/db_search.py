# coding: utf-8

# # Database tools
'''
A program to query the database for basic information about an item (or a bib record)

TO DO:
    - more sophisticated queries / for stats
    - csv output options
    - Alma data (via API?)

'''

import pyodbc
import pandas as pd
import webbrowser
import os
import time

# setup odbc connection:
dsn = 'DSN=Digitization-RDS;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'

# SET UP HTML STYLE

style = '''
html {
  font-size: 62.5%; }
body {
  font-size: 1.5em;
  line-height: 1.6;
  font-weight: 400;
  font-family: sans-serif;
  color: #222; 
  margin: 2em;  }

table, caption, tbody, tfoot, thead, tr, th, td {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent;
}
a {  transition: color .2s ease; text-decoration: none;  }
a:hover { text-decoration: underline; color: DarkSeaGreen; }
table { 
  table-layout: fixed;
  width: 100%;
}
 td {

    /* css-3 */
    white-space: -o-pre-wrap; 
    word-wrap: break-word;
    white-space: pre-wrap; 
    white-space: -moz-pre-wrap; 
    white-space: -pre-wrap; 
    width: 15em;

}
th, td { border-bottom: 1px solid DarkSeaGreen; padding: 4px; }

.hide_headers th.col_heading, .hide_headers th.blank { display: none }
h1, h2, h3, h4, h5, h6 {
  margin-top: 0;
  margin-bottom: 2rem;
  font-weight: 300;
}
tr:hover { background-color: PaleGreen }
h3 { display: inline; margin: 0; padding: 0 1em 0 0 ; clear: all; }
caption { font-size: 1.5em; padding: 1em; }
 
.container {   display: grid;
  grid-gap: 10px;
  grid-template-columns: 25em 25em 25em; padding-bottom: 1em;}


.box { border-left: 3px solid DarkSeaGreen; padding: 0 0 8px 1em;}
'''

def query_item_table(cursor, query):
    sql = '''
    SELECT 
        `i`.`UID` AS `UID`,
        `i`.`Bib_ID` AS `Bib_ID`,
        `t`.`Item_Entry_Type_Name`,
        `i`.`Barcode` AS `Barcode`,
        `i`.`Volume` AS `Volume`,
        `i`.`Copy` AS `Copy`,
        `i`.`Date_digitized` AS `Date_digitized`,
        `i`.`Number_of_images` AS `Number_of_images`,
        `e`.`Equipment_Name` AS `Equipment_name`,
        `i`.`Pullslip_Status` AS `Pullslip_Status`,
        `i`.`DSpace` AS `DSpace`,
        `i`.`IA` AS `IA`,
        `i`.`Bagged` AS `Bagged`,
        `i`.`Notes` AS `Notes`,
        `i`.`Collection_name` AS `Collection_name`,
        `ff`.`File_Format_Name` AS `Format_of_master`,
        `f`.`File_Format_Name` AS `Format_of_access`,
        `i`.`Phys_loc` AS `Phys_loc`,
        `i`.`Timestamp` AS `Timestamp`
    FROM
        (   (   (`Item_Entry` `i` LEFT JOIN `File_Formats` `f` ON `i`.`Format_of_access` = `f`.`File_Format_ID` )
            LEFT JOIN `Item_Entry_Types` `t` ON `i`.`Item_Type` = `t`.`Item_Entry_Type_ID` )
        LEFT JOIN `Equipment` `e` ON `i`.`Digitization_equipment` = `e`.`Equipment_ID` )
    LEFT JOIN `File_Formats` `ff` ON `i`.`Format_of_master` = `ff`.`File_Format_ID`'''

    if query.startswith('9910'):  # MMSID 
        sql += '''
    WHERE
        (`i`.`Bib_ID` = ?);
'''
    elif query.startswith('30700'):
        sql += '''
    WHERE
        (`i`.`Barcode` = ?);
'''
    else:  # assume it's a UID
        sql += '''
    WHERE
        (`i`.`UID` = ?);
'''
    cursor.execute(sql, query)
    return cursor.fetchall()

def query_bib_table(cursor, mmsid):
    sql = '''
    SELECT `p`.`Project_Name`,
        `b`.`Author`,
        `b`.`Title`,
        `b`.`Pub_Place`,
        `b`.`Imprint`,
        `b`.`PubYear`,
        `b`.`Edition`,
        `b`.`Pagination`,
        `b`.`Location`,
        `b`.`CallNumber`,
        `b`.`DG_URL`,
        `b`.`Notes`,
        `b`.`Foreign_Publisher`,
        `b`.`Right_to_left`,
        `b`.`Public_domain`,
        `b`.`Copyright_note`,
        `b`.`Timestamp`
    FROM
        (`Projects` `p` JOIN `Bib_Entry` `b` ON (`b`.`Project` = `p`.`Projects_ID`))
    WHERE (`b`.`Bibliographic_ID` = ?);
    '''
    cursor.execute(sql, mmsid)
    return cursor.fetchall()  


if __name__ == '__main__':
    query = None
    while True:
        textblocks = list()
        results = None
        bibresults = None
        iaresults = None
        mmsid = None
        displayframes = ''
        q = input('Enter barcode, UID, or MMSID: ').strip()
        if q:
            query = q
        cnxn = pyodbc.connect(dsn)
        cursor = cnxn.cursor()
        output = '<html><head><title>' + query + '</title><style>' + style + '</style></head><body>'   
        if query.startswith('9910'): # an MMSID
            mmsid = query
            bibresults = query_bib_table(cursor, query)
            if bibresults:
                bibcols = [c[0] for c in cursor.description]
                df_bib = pd.DataFrame.from_records(bibresults, columns=bibcols).transpose()
                df_bib_styler = df_bib.style.set_table_attributes('class="hide_headers" style="display:inline; padding: 0 20px ;"').set_caption('- Bib Table -')
                bib_item_results = query_item_table(cursor, query)
                columns = [c[0] for c in cursor.description]
                df_items = pd.DataFrame.from_records(bib_item_results, columns=columns).sort_values(by='Volume')
                df_item_styler = df_items.style.set_table_attributes('style="display:inline; padding: 0 20px ;"').set_caption('- Item Table -')
        else:
            results = query_item_table(cursor, query)
            if results:
                columns = [c[0] for c in cursor.description]
                # output = '<html><head><title>' + query + '</title><style>' + style + '</style></head><body>'
                df_item = pd.DataFrame.from_records(results, columns=columns).transpose()
                df_item_styler = df_item.style.set_table_attributes('class="hide_headers" style="display:inline;"').set_caption('- Item Table -')
                if results[0][1]:
                    mmsid = results[0][1]
                    bibresults = query_bib_table(cursor, mmsid)
                    bibcols = [c[0] for c in cursor.description]
                    if bibresults:
                        df_bib = pd.DataFrame.from_records(bibresults, columns=bibcols).transpose()
                        df_bib_styler = df_bib.style.set_table_attributes('class="hide_headers" style="display:inline; padding: 0 20px ;"').set_caption('- Bib Table -')            
                 # try to check IA table
                if results[0][0]:
                    sql = 'SELECT * FROM Internet_Archive WHERE IA_identifier=?'
                    cursor.execute(sql, (results[0][0]))
                    iaresults = cursor.fetchall()
                    if iaresults:
                        ia_cols = [column[0] for column in cursor.description]
                        df_ia = pd.DataFrame.from_records(iaresults, columns=ia_cols).drop(['Internet_Archive_auto_ID'], axis=1).transpose()
                        df_ia_styler = df_ia.style.set_table_attributes('class="hide_headers" style="display:inline;"').set_caption('- IA Table -')
                        if iaresults[0][11] is True: # add a thumbnail from IA if there is a PDF??
                            thumbnail = 'https://archive.org/download/' + results[0][0] + '/page/title_w100.jpg'
                            textblocks.append('<img src="' + thumbnail + '">')
                        else:
                            textblocks.append('<em>Item data exists on IA, but there is no PDF file (never scanned or uploaded?)</em>')
                        htm = '<h3>Internet Archive links:</h3>'
                        htm += '<nav><a href="http://archive.org/details/' + results[0][0] + '"><strong>' + results[0][0] + '</strong></a> • '
                        htm += '<a href="https://catalogd.archive.org/history/' + results[0][0] + '">Item history</a> • '
                        htm += '<a href="https://archive.org/editxml/' + results[0][0] + '">Metadata editor</a> • '
                        htm += '<a href="https://archive.org/edit.php?edit-files=1&identifier=' + results[0][0] + '">File editor</a> • '
                        htm += '<a href="https://archive.org/download/' + results[0][0] + '">Files</a></nav>'
                        textblocks.append(htm)
                    else:
                        textblocks.append('<em>Not found in Internet Archive table.</em>')
        if mmsid:
            htm = '<h3>Primo link:</h3>'
            primo = 'https://wrlc-gulaw.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma' + mmsid + '&vid=01WRLC_GUNIVLAW:01WRLC_GUNIVLAW'
            htm += '<nav><a href="' + primo + '">Display in Primo</a></nav>'
            textblocks.append(htm)
        if results or bibresults:
            if query.startswith('9910'): # an MMSID
                displayframes += df_bib_styler._repr_html_()
                displayframes += df_item_styler._repr_html_()
            else:
                displayframes = df_item_styler._repr_html_()
                if bibresults:
                    displayframes += df_bib_styler._repr_html_()
                if iaresults:
                    displayframes += df_ia_styler._repr_html_()
            if textblocks:
                output += '<div class="container">'
                for _ in textblocks:
                    output += '<div class="box">' + _ + '</div>'
                output += '</div>'
            output += displayframes
            output += '<p><small><strong>Data accurate as of</strong> ' + time.ctime() + '</p></body></html>'
            os.makedirs('output', exist_ok=True)
            with open('output/output.html', 'w', encoding='utf-8') as f:
                url = 'file://' + os.path.join(os.getcwd(), 'output/output.html')
                f.write(output)
                webbrowser.open(url)
        else:
            print('\nNot found!\n')

