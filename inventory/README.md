# Inventory module

In the past, we've tried out using Google Forms for inventory ([demo](https://docs.google.com/forms/d/1nMpn6qDX3V8R-kgJ7MkzCps2EMuBCNsn_pbMd52znXM/edit)/[results](https://docs.google.com/spreadsheets/d/1H4I8ACTYEJ1w_7l0SzamBbLYAcBN9qLsSsNGxMzZgl4/edit?usp=sharing)).  This has not been super satisfactory, because (1) the wi-fi is spotty, and (2) the Google Forms are tedious. You need to have a pre-defined number of "questions" and hit tab between each barcode entry. (Sidenote: you can, however, validate the barcodes in Google Forms via regex like `Regular expression MATCHES 30700\d{9}`.)

This really calls for a simple webapp that can verify valid barcodes and store them. However, I put together a jupyter notebook to achieve the same results -- it's basically an app with the code exposed.

This module includes a batch script that:

- creates a folder in the Windows *public user* account;
- copies over the directory containing the batchscript via `robocopy`;
- activates anaconda/miniconda environment;
- launches jupyter. 

There is also a pre-defined list of shelves in a "docs" subfolder.

### Setup

I envision this to be run via downloading the directory from Bitbucket or Box, then running the batch script to copy files into the Windows public user account and launching jupyter. It should be very lite to set up, except for installing Python and Jupyter.

One could add a desktop shortcut for the `start.bat` file. 

The notebook itself can run independently of the batch script.

### Usage

When the batch file runs, it will erase any data saved in the Jupyter notebook in the public user directory, then open up that directory in Jupyter. 

A user can simply click on the inventory `.ipynb` file and run the notebook.

There is just one code block; click inside it so that a cursor is operating, then hit SHIFT-ENTER to run it. It asks for a user to input a shelf by name, and then allows for the entry of barcodes, one by one. It does basic validation of the barcodes and emits a Windows alert sound on invalid entry. 

If the operator enters a valid shelf number, then the program saves the existing list of barcodes as a csv, and then begins a new list for the new shelf.

The csvs are saved in an output folder with filenames based on the date. The csv content is in the format: SHELF,BARCODE,TIMESTAMP

### Requirements

- Python3 (optimized for [miniconda](https://conda.io/docs/user-guide/install/index.html) in the batch script.)
- Jupyter
- Windows (works on 7 and 10). If you delete the parts about winsound, and ignore the batch script, it's most likely fine cross-platform.
