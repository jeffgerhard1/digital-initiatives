@echo off
goto :TopOfCode

====================================================
Batch script to set up folders and copy this directory
to a public user account via robocopy. 
(Maybe you got this directory from the git repository,
or maybe from Box or elsewhere).

Tries to detect the Python install if it is via Anaconda/Miniconda
and use "call" statements to activate the conda environment
and then run jupyter within the public user directory.

Can make a shortcut to this file (NOT THE DUPLICATE ONE IN THE PUBLIC DIRECTORY)
and have it on the desktop.

If running this where python 2 is the default,
add the following lines to the TopOfCode section:

python3 -m pip install ipykernel
python3 -m ipykernel install --user

====================================================


:TopOfCode
@echo on
if exist %LOCALAPPDATA%\Continuum\anaconda3\ set root=%LOCALAPPDATA%\Continuum\anaconda3
if exist %LOCALAPPDATA%\Continuum\miniconda3\ set root=%LOCALAPPDATA%\Continuum\miniconda3
if exist %UserProfile%\Miniconda3\ set root=%UserProfile%\Miniconda3
call %root%\Scripts\activate.bat %root%
if not exist %PUBLIC%\inventory_notebook\ mkdir %PUBLIC%\inventory_notebook
if not exist %PUBLIC%\inventory_notebook\docs mkdir %PUBLIC%\inventory_notebook\docs
if not exist %PUBLIC%\inventory_notebook\output mkdir %PUBLIC%\inventory_notebook\output
cd /d %~dp0
robocopy . %PUBLIC%\inventory_notebook /e /XD output .ipynb_checkpoints __pycache__
cd /d %PUBLIC%\inventory_notebook
call jupyter lab
