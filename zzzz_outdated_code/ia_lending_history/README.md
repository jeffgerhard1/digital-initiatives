# This method of gathering statistics is now out of date (as of June 2019)

See the `loanhistory` functionality in [ia_metadata](https://bitbucket.org/jeffgerhard1/digital-initiatives/src/master/ia_metadata/) instead.

.

.

.

.

.

----

_old info below_:

# Generate loan history for Controlled Digital Lending items on IA


Please see the background discussion about this script on the related [Google Doc](https://docs.google.com/document/d/1A2_WuHkrKsYLncMzWkRFUGDXMUngThNsyVLotGGk4A0/edit?usp=sharing).

## Overview

This is a simple Python script that parses the saved JSON files from the [IA Metadata](https://bitbucket.org/jeffgerhard1/digital-initiatives/src/master/ia_metadata/) program. 

What this code does is:

- Looks through a folder for JSON files with IA metadata for our items (assumes that there is a local folder like via Google Drive File Stream).
- Compiles all instances of a `loans__status__last_loan_date` for each item
- Saves a csv file with compiled data.

## Requirements

Python 3
