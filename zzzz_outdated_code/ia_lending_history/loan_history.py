# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 2018

A process for gathering stats from the old json files:

- open each json file
- look for "loans__status__last_loan_date" in each item
- if exists, add it to a list of loans in a dictionary of items IF not already in the loan history

This results in a dictionary of items that have loan histories and loan dates

The resulting dictionary looks like this:
    
{'1998200_xxx_2001_00_0125': ['2017-11-21T19:20:45Z',
                              '2017-05-12T12:02:20Z',
                              '2017-06-27T19:28:31Z'],
 'aboutcr_xxx_2012_00_0503': ['2018-03-16T17:17:37Z'],
 'academicinst_garb_2001_000_7677585': ['2017-12-08T19:11:39Z'],
 'admiral_mar_2005_00_5998': ['2017-04-13T19:13:05Z'],
 'beyondbelief_bell_1991_000_4394481': ['2018-01-29T06:00:31Z',
                                        '2018-11-06T14:10:51Z'],
 ... [etc]
 }

I take that dictionary and make a csv so that I can import it into Google Sheets...
the csv ends up with the format:
    
    identifier,count of loans,semicolon-separated list of loan dates

"""

import os
import json


def list_files(d, filetype='json'):
    '''return a list of json files in a directory'''
    _ = []
    for file in os.listdir(d):
        if os.path.splitext(file)[1][1:] == filetype:
            _.append(file)
    return _

def parse_items(f):
    ''' return a dictionary of any loans recorded in this json file '''
    quickresults = dict()
    with open(f, 'r', encoding='utf-8') as fh:
        info = json.loads(fh.read())
        for item in info:
            if 'loans__status__last_loan_date' in item:
                quickresults[item['identifier']] = item['loans__status__last_loan_date']
    return quickresults
                

filedir = r'G:\Team Drives\Digital Initiatives\Internet Archive\IA metadata\metadata history'

realresults = dict()  # this is the dictionary with the final results

files = list_files(filedir)

for f in files:
    r = parse_items(os.path.join(filedir, f))
    for item in r:
        if item in realresults:
            if r[item] not in realresults[item]:
                realresults[item].append(r[item])
        else:
            realresults[item] = [r[item]]

# ok let's make a csv for export to google sheetz
# something like item, total checkouts, checkout_dates (sorted)

csvtext = ''
for item in realresults:
    csvtext += item + ','
    csvtext += str(len(realresults[item]))
    csvtext += ','
    _ = sorted(realresults[item], reverse=True)
    csvtext += '; '.join(_)
    csvtext += '\n'
    
with open(os.path.join(filedir, 'circ_stat.csv'), 'w', encoding='utf-8') as fh:
    fh.write(csvtext)

