# Download and bag IA archival masters

Please see the background discussion about this script on the related [Google Doc](https://docs.google.com/document/d/1lC0mgAR-Yr6BK6QJk7AiKwcy3PSdvjQKYBoxSpaed0s/edit?usp=sharing).

## Overview

This is an old piece of code, written in Python 3 while I was just learning how to use Python effectively.

Basically, it reads a list of Internet Archive identifiers saved from a file (you might get such a list out of the digitization database). Then it attempts to download a tar file of jp2s. You need to be logged in with the `ia` command-line tool to have access to these items online.

On download, the script does a basic fixity analysis, comparing the md5 checksums to the ones listed on the IA, then bags the downloaded files.

## Requirements

Python 3, including non-standard modules:

- [internetarchive](https://github.com/jjjake/internetarchive)
- [bagit-python](https://github.com/LibraryOfCongress/bagit-python)

