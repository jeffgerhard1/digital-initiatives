'''

An old script from when I was first learning how to really use Python
For Python 3; requires internetarchive and bagit modules

This requires a list of IA identifiers, one per line,
and attempts to download an "orig_jp2.tar" from the Internet Archive
(along with any bibliographic metadata records such as a marcxml file.)

On donwload, it does a quick checksum analysis to make sure the md5 checksum
matches the one listed on IA. On mismatch, it re-runs the download.

If the download is successful, this will bag the downloads.

'''

from internetarchive import download, get_item
import os
from tkinter.filedialog import askopenfilename
import bagit
import hashlib

def dname(x):
        return "Z:\\holding\\books\\IA_download\\" + x + "_master"
    
def localfile(x):
        return dname(x) + "\\" + x + "_orig_jp2.tar"
    
def masterfile(x):
        return dname(x) + "\\data\\" + x + "_orig_jp2.tar"
    
def grabFromIA(x,check=True):
        dir = dname(x)
        os.makedirs(dir, exist_ok=True)
        os.chdir(dir)
        for p in ["*marc.xml","*metasource.xml","*orig_jp2.tar"]:
                download(x, verbose=True, checksum=check, glob_pattern=p, no_directory=True, retries=10)
        if not confirmmd5(x):
                print('\nALERT: checksum mismatch; re-attempting download...\n')
                grabFromIA(x,check=False)
                # i should add a number of tries functionality to this before i leave this, like, running for a weekend
                
def getinfo(x):
        files = get_item(x).files
        tar = x + "_orig_jp2.tar"
        for z in files:
                if z['name'] == tar:
                        info = z
# problem here -- check for items without tar // ones we uploaded in unusual way!!!
        size = info['size']
        md5 = info['md5']
        sha1 = info['sha1']
        return [humanbytes(size),md5,sha1,size,tar]
    
def humanbytes(B):
   'Return the given bytes as a human friendly KB, MB, GB, or TB string'
#via http://stackoverflow.com/a/31631711
   B = float(B)
   KB = float(1024)
   MB = float(KB ** 2) # 1,048,576
   GB = float(KB ** 3) # 1,073,741,824
   TB = float(KB ** 4) # 1,099,511,627,776

   if B < KB:
      return '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
   elif KB <= B < MB:
      return '{0:.2f} KB'.format(B/KB)
   elif MB <= B < GB:
      return '{0:.2f} MB'.format(B/MB)
   elif GB <= B < TB:
      return '{0:.2f} GB'.format(B/GB)
   elif TB <= B:
      return '{0:.2f} TB'.format(B/TB)
  
def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def confirmmd5(x):
        i = getinfo(x)
        print('\n\ncalculating and comparing checksums...(' + i[0] + ')')
        iam = i[1]
        print('remote md5:\t' + str(iam))
        m = md5(localfile(x))
        print('local md5:\t' + str(m))
        return m == iam
    
def checkforbag(x):
        if os.path.isfile(masterfile(x)):
                if os.path.isfile(dname(x) + "\\bagit.txt"):
                        b = bagit.Bag(dname(x))
                        return b.is_valid(fast=True)
        else:
                return False
            
fname = askopenfilename(title='Choose a list of IA identifiers',initialdir='Z:\\holding\\books\\IA_download\\')
with open(fname, "r") as fh:
        lines = fh.read()
for x in lines.splitlines():
        print('\n\n','{:*>60}'.format(' '+x),'\n\n')
        if os.path.isfile(localfile(x)):
                if not confirmmd5(x):
                        grabFromIA(x, check=False)
        elif not os.path.isfile(masterfile(x)):
                print('Downloading... ',getinfo(x)[0])
                grabFromIA(x)
        else:
                print('(skipping)')
        if not checkforbag(x):
                print('\n\nBagging',dname(x))
                bag = bagit.make_bag(dname(x),checksum=['sha1'])