# -*- coding: utf-8 -*-
"""
# Extract jpg's from pdf's. Quick and dirty.
via https://nedbatchelder.com/blog/200712/extracting_jpgs_from_pdfs.html
 and particularly, comment by Ryan [4:36 AM on 10 Aug 2016]
"""

import sys



startmark = b"\xff\xd8"
startfix = 0
endmark = b"\xff\xd9"
endfix = 2
i = 0

njpg = 0

if __name__ == "__main__":
    with open(sys.argv[1], "rb") as file:
        pdf = file.read()
        while True:
            istream = pdf.find(b"stream", i)
            if istream < 0:
                break
            istart = pdf.find(startmark, istream, istream + 20)
            if istart < 0:
                i = istream + 20
                continue
            iend = pdf.find(b"endstream", istart)
            if iend < 0:
                raise Exception("Didn't find end of stream!")
            iend = pdf.find(endmark, iend - 20)
            if iend < 0:
                raise Exception("Didn't find end of JPG!")

            istart += startfix
            iend += endfix
            print("JPG %d from %d to %d" % (njpg, istart, iend))
            jpg = pdf[istart:iend]
            with open(str(njpg).zfill(4) + '.jpg', "wb") as jpgfile:
                jpgfile.write(jpg)
        
            njpg += 1
            i = iend