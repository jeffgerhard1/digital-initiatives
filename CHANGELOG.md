# Changelog

## 2020-07-13

### Updated:

* Cleanup activity and transfer of this repo to organizational workspace on Bitbucket

## 2020-03-06

### Added:

* Initial implementation of digital representations via Alma API

## 2020-02-21

### Updated:

* Continued to build out stats into some jupyter notebooks that can push 
stats into a Google Sheet

## 2020-02-03

### Added:

* New stats-gathering notebooks

### Updated:

* Revised a lot of metadata code, including some work to deal with revised 
Internet Archive tasks API

## 2019-09-27

### Updated:

* Revising the inventory module to make it more rugged (i.e., appending each barcode to a file rather than writing all at once)

## 2019-09-18

### Added:

* Early versions of some reporting/stats tools in a `database_tools_and_stats` folder

### Updated:

* Added some new functionality to the `ia_metadata` scripts to look at file data (i.e., what kinds of files are in our IA items and which are original vs. derived formats)

## 2019-06-21

### Added:

* Entire new functionality for dealing with IA metadata. Goes with the new database. Tracks all of the following via the `ia_metadata.py` script:
** metadata
** identifiers
** loanhistory
** loanstatus
** views

* A folder for some utilities scripts, so far with just one thing (a PDF-image extractor)
