# Digital Initiatives

## Introduction

This is an attempt to organize, document, and use better version control for departmental scripts. This is mainly in Python but also includes Windows batch scripts and other pieces of code.

### Setting up

Everything is written assuming a Python 3 environment. I recommend installing Python via Anaconda or (even better) [miniconda](https://docs.conda.io/en/latest/miniconda.html). 

#### Downloading this repository

To install this code, hit the "Clone" button in the upper right, copy the text, and then clone via a utility like git-bash. 

#### Requirements

You can update your Python modules to the required levels by running the following command in the same directory as this README file and the `requirements.txt` file:

`pip install -r requirements.txt`

### Using this code

Some of the code in this repository is meant to be run from the command line like this:

`python db_search.py`

And some of it consists of Jupyter notebooks that you can run in Jupyter Lab. You might want to run Jupyter Lab anyway; it's a good platform for viewing markdown files (right-click on the file --> open with --> markdown preview), csv output files, etc. For example, you can launch Jupyter Lab in the main directory (where this README file lives) like so:

```
activate
call jupyter lab
```
## Contents


### Inventory

Code to [run inventory](inventory) in a Juptyter notebook. Intended for use in the holding shelves and storage areas.

### Database tools and stats

Code to [query the database](database_tools_and_stats) and see useful 
results. Includes notebooks that can help push data into Google Sheets for 
easier snapshots (compared to dealing with the database directly).

### Metadata

The code used to [pull information from the Internet Archive](ia_metadata) and populate the database.

### Utilities

A general bucket of [useful utilities](utilities) and code (not much here as of now).

### [Downloading archival masters](ia_download_arch_masters) from the IA

Not currently in use but may be useful again in future.

### Outdated code

Older methods of accomplishing some of the above.
